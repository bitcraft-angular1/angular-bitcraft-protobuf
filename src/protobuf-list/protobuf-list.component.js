/*global angular */
'use strict';

angular.
    module('bitcraft-protobuf.list').
    component('bitcraftProtobufList', {
        templateUrl: 'js/protobuf/protobuf-list/protobuf-list.template.html',
        controller: function ProtobufListController() {
            var self = this;

            self.versions = [];

            self.$onChanges = function (changesObj) {
                if (changesObj.protobufList) {
                    if (changesObj.protobufList.isFirstChange()) {
                        self.versions = angular.copy(changesObj.protobufList.currentValue);
                        return;
                    }
                    self.protobufList = angular.copy(changesObj.protobufList.currentValue);
                    self.versions = angular.copy(changesObj.protobufList.currentValue);
                }
            };
        },
        bindings: {
            protobufList: '<',
            onDelete: '&',
            onDownload: '&',
            onActivate: '&',
            onUpdateComment: '&'
        }
    });
