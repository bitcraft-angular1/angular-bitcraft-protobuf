/*global angular, window*/
'use strict';

angular.
    module('bitcraft-protobuf').
    factory('Protobuf', [
        '$http', '$log', '$q', 'Upload',
        function ($http, $log, $q, Upload) {
            /**
             * Returns a promise with all the protobuf
             * @returns {Promise}
             */
            function getAll() {
                var deferred = $q.defer();

                $http.get('./rest/listProtobuf').then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the protobuf list (' + resp.statusText + ')');
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Trigger the download of the protobuf with the given version
             * @param {string} version
             */
            function downloadVersion(version) {
                var url = './rest/downloadProtobuf?version=' + version;
                window.open(url, '_blank');
            }

            /**
             * Update the comment of the protobuf with the given version
             * @param {number} version
             * @param {string} comment
             * @returns {*}
             */
            function updateComment(version, comment) {
                var req = {
                    method: 'POST',
                    url: './rest/updateProtobufComment',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {version: version, comment: comment}
                };

                return $http(req);
            }

            /**
             * Delete the protobuf with the given version
             * @param {string} version
             */
            function deleteProtobuf(version) {
                var req = {
                    method: 'POST',
                    url: './rest/deleteProtobuf',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {version: version}
                };

                return $http(req);
            }

            /**
             * Activate the protobuf with the given version
             * @param {number} version
             */
            function activateProtobuf(version) {
                var req = {
                    method: 'POST',
                    url: './rest/activateProtobuf',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {version: version}
                };

                return $http(req);
            }

            /**
             * @param {Array.<File>} files
             */
            function upload(files) {
                return Upload.upload({
                    url: './rest/uploadProtobuf',
                    data: {
                        file: files
                    }
                });
            }

            return {
                getAll: getAll,
                updateComment: updateComment,
                downloadVersion: downloadVersion,
                deleteProtobuf: deleteProtobuf,
                activateProtobuf: activateProtobuf,
                upload: upload
            };
        }
    ]);
