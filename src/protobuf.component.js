/*global angular*/
'use strict';

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-protobuf').
    component('bitcraftProtobuf', {
        templateUrl: 'js/protobuf/protobuf.template.html',
        controller: [
            '$scope', '$log', 'Notification', 'dialogs', 'Protobuf', 'Faye',
            function protobufController(
                $scope,
                $log,
                Notification,
                dialogs,
                Protobuf,
                Faye
            ) {
                var self = this;

                // faye
                var fayeChannel = '/logChannel';
                var fayeSubscriptions = {};
                var logCategory = 'protobuf';

                /**
                 * @param {string} line
                 */
                function appendLog(line) {
                    self.log = line + '\n' + self.log;
                }

                function refresh() {
                    Protobuf.getAll().then(function (data) {
                        self.versions = data;
                    }, function (resp) {
                        Notification.error({
                            message: 'failed to retrieve version history (' + resp.statusText + ')'
                        });
                        appendLog('ERROR: Failed to retrieve version history');
                    });
                }

                self.onDownload = Protobuf.downloadVersion;

                /**
                 * @param {number} version
                 */
                self.onActivate = function (version) {
                    Protobuf.activateProtobuf(version).then(function () {
                        Notification.success(
                            {message: 'protobuf version ' + version + ' activated'}
                        );
                        refresh();
                    }, function (resp) {
                        Notification.error({
                            message: 'failed to activate the version ' + version + ' (' + resp.statusText + ')'
                        });
                        refresh();
                    });
                };

                /**
                 * @param {number} version
                 * @param {string} comment
                 */
                self.updateComment = function (version, comment) {
                    Protobuf.updateComment(version, comment).then(function () {
                        $log.info('SUCCESS: comment update successful');
                        Notification.success({message: 'comment update successful'});
                    }, function (resp) {
                        $log.error('ERROR: comment update failed');
                        Notification.error(
                            {message: 'comment update failed (' + resp.statusText + ')'}
                        );
                        refresh();
                    });
                };

                /**
                 * @param {number} version
                 */
                self.onDelete = function (version) {
                    var dlg = dialogs.confirm(
                        'Confirmation required',
                        'Do you want to delete this version?',
                        {size: 'sm'}
                    );
                    dlg.result.then(function (btn) {
                        Protobuf.deleteProtobuf(version).then(function () {
                            $log.info('SUCCESS: version deleted');
                            Notification.success({message: 'version ' + version + ' deleted'});
                            refresh();
                        }, function (resp) {
                            $log.error('ERROR: deletion failed');
                            Notification.error(
                                {message: 'deletion failed (' + resp.statusText + ')'}
                            );
                        });

                    }, function (btn) {
                        $log.info('Version deletion cancelled.');
                    });
                };

                var notifyCallback = function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total, 10);

                    // we only display progression feedback when the upload takes time,
                    // so we discard the 100% notification for instantaneous upload
                    if (progressPercentage !== 100) {
                        $log.info('PROGRESS: ' + progressPercentage + '% ');
                    }
                };

                self.onUpload = function (filesData) {
                    $log.info('protobuf on upload');
                    if (filesData && filesData.length > 0) {
                        Protobuf.upload(filesData).then(function () {
                            $log.info('upload success');
                            Notification.success({message: 'upload success'});
                            refresh();
                        }, function (resp) {
                            $log.error('upload failed ', resp.statusText);
                            Notification.error(
                                {message: 'upload failed (' + resp.statusText + ')'}
                            );
                        }, notifyCallback);
                    }
                };

                // log
                self.log = '';
                self.appendLog = appendLog;

                self.clearLogs = function () {
                    self.log = '';
                };

                // faye
                // Listen to data coming from the server via Faye
                fayeSubscriptions.channel = Faye.subscribe(fayeChannel,
                    /**
                     * @param {object} context (not used)
                     * @param {logMessage} message
                     */
                    function (context, message) {
                        if (message.logger.category === logCategory) {
                            appendLog('SERVER: ' + message.data.join(' '));
                        }
                    },
                    false);

                // clean up
                $scope.$on('$destroy', function () {
                    $log.info('Cleaning up Faye subscriptions');
                    fayeSubscriptions.channel.unsubscribe(false);
                });

                refresh();

                appendLog('Ready for upload!');
            }
        ]
    });
