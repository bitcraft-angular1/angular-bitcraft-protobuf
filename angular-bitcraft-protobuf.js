(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';
require('./protobuf.module');
require('./protobuf.service');
require('./protobuf.component');
require('./protobuf-list');

},{"./protobuf-list":2,"./protobuf.component":5,"./protobuf.module":6,"./protobuf.service":7}],2:[function(require,module,exports){
'use strict';
require('./protobuf-list.module');
require('./protobuf-list.component');

},{"./protobuf-list.component":3,"./protobuf-list.module":4}],3:[function(require,module,exports){
/*global angular */
'use strict';

var template = '' +
'<table st-table="versions" st-safe-src="$ctrl.protobufList" class="table table-striped">' +
'    <thead>' +
'    <tr>' +
'        <th st-sort="version" st-sort-default="reverse" translate>PROTOBUF.VERSION</th>' +
'        <th translate>PROTOBUF.DATE</th>' +
'        <th translate>PROTOBUF.STATUS</th>' +
'        <th translate>PROTOBUF.ACTIONS</th>' +
'        <th translate>PROTOBUF.COMMENT</th>' +
'    </tr>' +
'    </thead>' +
'    <tbody>' +
'    <tr ng-repeat="item in versions" ng-class="{info: item.active}">' +
'        <td>{{item.version}}</td>' +
'        <td>{{item.date | date : \'short\'}}</td>' +
'        <td ng-if="item.active" translate>GENERAL.ACTIVE</td>' +
'        <td ng-if="!item.active" translate>GENERAL.INACTIVE</td>' +
'        <td>' +
'            <button type="button"' +
'                    class="btn btn-success"' +
'                    ng-disabled="item.active"' +
'                    ng-click="$ctrl.onActivate({version: item.version})" translate>' +
'                PROTOBUF.ACTIVATE' +
'            </button>' +
'            <button type="button"' +
'                    class="btn btn-primary"' +
'                    ng-click="$ctrl.onDownload({version: item.version})" translate>' +
'                GENERAL.DOWNLOAD' +
'            </button>' +
'            <button type="button"' +
'                    class="btn btn-danger"' +
'                    ng-click="$ctrl.onDelete({version: item.version})" translate>' +
'                GENERAL.DELETE' +
'            </button>' +
'        </td>' +
'        <!-- editable comment -->' +
'        <td>' +
'            <a href="#"' +
'               editable-text="item.comment"' +
'               onaftersave="$ctrl.onUpdateComment({version: item.version, comment: $data})">' +
'                {{ item.comment || \'empty\' }}' +
'            </a>' +
'        </td>' +
'    </tr>' +
'    </tbody>' +
'    <tfoot>' +
'    <tr>' +
'        <td colspan="5" class="text-center">' +
'            <div st-pagination="" st-items-by-page="10" st-displayed-pages="7"></div>' +
'        </td>' +
'    </tr>' +
'    </tfoot>' +
'</table>' +
'';


angular.
    module('bitcraft-protobuf.list').
    component('bitcraftProtobufList', {
        template: template,
        controller: function ProtobufListController() {
            var self = this;

            self.versions = [];

            self.$onChanges = function (changesObj) {
                if (changesObj.protobufList) {
                    if (changesObj.protobufList.isFirstChange()) {
                        self.versions = angular.copy(changesObj.protobufList.currentValue);
                        return;
                    }
                    self.protobufList = angular.copy(changesObj.protobufList.currentValue);
                    self.versions = angular.copy(changesObj.protobufList.currentValue);
                }
            };
        },
        bindings: {
            protobufList: '<',
            onDelete: '&',
            onDownload: '&',
            onActivate: '&',
            onUpdateComment: '&'
        }
    });

},{}],4:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('bitcraft-protobuf.list', []);

},{}],5:[function(require,module,exports){
/*global angular*/
'use strict';

var template = '' +
'<div class="row">' +
'    <div class="col-md-5">' +
'        <div class="row">' +
'            <div class="col-md-12" style="margin-top:10px; margin-bottom:10px;">' +
'                <h4 translate>PROTOBUF.FILE_UPLOAD</h4>' +
'            </div>' +
'        </div>' +
'        <div class="row">' +
'            <div class="col-md-12">' +
'                <files-upload' +
'                        on-upload="$ctrl.onUpload(data)"' +
'                        format-name="proto"' +
'                        extension=".proto"' +
'                        allow-cancel="false"' +
'                ></files-upload>' +
'            </div>' +
'        </div>' +
'        <hr>' +
'        <div class="row">' +
'            <div class="col-md-12">' +
'                <div class="row">' +
'                    <div class="col-md-3">' +
'                        <h4 translate>PROTOBUF.PROCESSING_LOG</h4>' +
'                    </div>' +
'                    <div class="col-md-9">' +
'                        <div class="btn btn-primary" style="margin-left: 0;" ng-click="$ctrl.clearLogs()" translate>PROTOBUF.CLEAR</div>' +
'                    </div>' +
'                </div>' +
'                <pre style="margin-top: 10px;">{{$ctrl.log}}</pre>' +
'            </div>' +
'        </div>' +
'    </div>' +
'    <div class="col-md-7">' +
'        <div class="row" style="margin-top:10px; margin-bottom:10px;">' +
'            <div class="col-md-12">' +
'                <h4 translate>PROTOBUF.UPLOAD_HISTORY</h4>' +
'            </div>' +
'        </div>' +
'        <bitcraft-protobuf-list protobuf-list="$ctrl.versions"' +
'                       on-delete="$ctrl.onDelete(version)"' +
'                       on-download="$ctrl.onDownload(version)"' +
'                       on-activate="$ctrl.onActivate(version)"' +
'                       on-update-comment="$ctrl.updateComment(version, comment)"></bitcraft-protobuf-list>' +
'    </div>' +
'</div>' +
'';


//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-protobuf').
    component('bitcraftProtobuf', {
        template: template,
        controller: [
            '$scope', '$log', 'Notification', 'dialogs', 'Protobuf', 'Faye',
            function protobufController(
                $scope,
                $log,
                Notification,
                dialogs,
                Protobuf,
                Faye
            ) {
                var self = this;

                // faye
                var fayeChannel = '/logChannel';
                var fayeSubscriptions = {};
                var logCategory = 'protobuf';

                /**
                 * @param {string} line
                 */
                function appendLog(line) {
                    self.log = line + '\n' + self.log;
                }

                function refresh() {
                    Protobuf.getAll().then(function (data) {
                        self.versions = data;
                    }, function (resp) {
                        Notification.error({
                            message: 'failed to retrieve version history (' + resp.statusText + ')'
                        });
                        appendLog('ERROR: Failed to retrieve version history');
                    });
                }

                self.onDownload = Protobuf.downloadVersion;

                /**
                 * @param {number} version
                 */
                self.onActivate = function (version) {
                    Protobuf.activateProtobuf(version).then(function () {
                        Notification.success(
                            {message: 'protobuf version ' + version + ' activated'}
                        );
                        refresh();
                    }, function (resp) {
                        Notification.error({
                            message: 'failed to activate the version ' + version + ' (' + resp.statusText + ')'
                        });
                        refresh();
                    });
                };

                /**
                 * @param {number} version
                 * @param {string} comment
                 */
                self.updateComment = function (version, comment) {
                    Protobuf.updateComment(version, comment).then(function () {
                        $log.info('SUCCESS: comment update successful');
                        Notification.success({message: 'comment update successful'});
                    }, function (resp) {
                        $log.error('ERROR: comment update failed');
                        Notification.error(
                            {message: 'comment update failed (' + resp.statusText + ')'}
                        );
                        refresh();
                    });
                };

                /**
                 * @param {number} version
                 */
                self.onDelete = function (version) {
                    var dlg = dialogs.confirm(
                        'Confirmation required',
                        'Do you want to delete this version?',
                        {size: 'sm'}
                    );
                    dlg.result.then(function (btn) {
                        Protobuf.deleteProtobuf(version).then(function () {
                            $log.info('SUCCESS: version deleted');
                            Notification.success({message: 'version ' + version + ' deleted'});
                            refresh();
                        }, function (resp) {
                            $log.error('ERROR: deletion failed');
                            Notification.error(
                                {message: 'deletion failed (' + resp.statusText + ')'}
                            );
                        });

                    }, function (btn) {
                        $log.info('Version deletion cancelled.');
                    });
                };

                var notifyCallback = function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total, 10);

                    // we only display progression feedback when the upload takes time,
                    // so we discard the 100% notification for instantaneous upload
                    if (progressPercentage !== 100) {
                        $log.info('PROGRESS: ' + progressPercentage + '% ');
                    }
                };

                self.onUpload = function (filesData) {
                    $log.info('protobuf on upload');
                    if (filesData && filesData.length > 0) {
                        Protobuf.upload(filesData).then(function () {
                            $log.info('upload success');
                            Notification.success({message: 'upload success'});
                            refresh();
                        }, function (resp) {
                            $log.error('upload failed ', resp.statusText);
                            Notification.error(
                                {message: 'upload failed (' + resp.statusText + ')'}
                            );
                        }, notifyCallback);
                    }
                };

                // log
                self.log = '';
                self.appendLog = appendLog;

                self.clearLogs = function () {
                    self.log = '';
                };

                // faye
                // Listen to data coming from the server via Faye
                fayeSubscriptions.channel = Faye.subscribe(fayeChannel,
                    /**
                     * @param {object} context (not used)
                     * @param {logMessage} message
                     */
                    function (context, message) {
                        if (message.logger.category === logCategory) {
                            appendLog('SERVER: ' + message.data.join(' '));
                        }
                    },
                    false);

                // clean up
                $scope.$on('$destroy', function () {
                    $log.info('Cleaning up Faye subscriptions');
                    fayeSubscriptions.channel.unsubscribe(false);
                });

                refresh();

                appendLog('Ready for upload!');
            }
        ]
    });

},{}],6:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('bitcraft-protobuf', ['bitcraft-protobuf.list', 'core.filesUpload', 'ngFileUpload']);

},{}],7:[function(require,module,exports){
/*global angular, window*/
'use strict';

angular.
    module('bitcraft-protobuf').
    factory('Protobuf', [
        '$http', '$log', '$q', 'Upload',
        function ($http, $log, $q, Upload) {
            /**
             * Returns a promise with all the protobuf
             * @returns {Promise}
             */
            function getAll() {
                var deferred = $q.defer();

                $http.get('./rest/listProtobuf').then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the protobuf list (' + resp.statusText + ')');
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Trigger the download of the protobuf with the given version
             * @param {string} version
             */
            function downloadVersion(version) {
                var url = './rest/downloadProtobuf?version=' + version;
                window.open(url, '_blank');
            }

            /**
             * Update the comment of the protobuf with the given version
             * @param {number} version
             * @param {string} comment
             * @returns {*}
             */
            function updateComment(version, comment) {
                var req = {
                    method: 'POST',
                    url: './rest/updateProtobufComment',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {version: version, comment: comment}
                };

                return $http(req);
            }

            /**
             * Delete the protobuf with the given version
             * @param {string} version
             */
            function deleteProtobuf(version) {
                var req = {
                    method: 'POST',
                    url: './rest/deleteProtobuf',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {version: version}
                };

                return $http(req);
            }

            /**
             * Activate the protobuf with the given version
             * @param {number} version
             */
            function activateProtobuf(version) {
                var req = {
                    method: 'POST',
                    url: './rest/activateProtobuf',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {version: version}
                };

                return $http(req);
            }

            /**
             * @param {Array.<File>} files
             */
            function upload(files) {
                return Upload.upload({
                    url: './rest/uploadProtobuf',
                    data: {
                        file: files
                    }
                });
            }

            return {
                getAll: getAll,
                updateComment: updateComment,
                downloadVersion: downloadVersion,
                deleteProtobuf: deleteProtobuf,
                activateProtobuf: activateProtobuf,
                upload: upload
            };
        }
    ]);

},{}]},{},[1]);
